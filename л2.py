Python 3.9.7 (tags/v3.9.7:1016ef3, Aug 30 2021, 20:19:38) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import random
>>> x = [random.randrange(20) for i in range(10)]
>>> y = [random.randrange(20) for i in range(10)]
>>> coord = list(zip(x,y))
>>> coord = [list(row) for row in coord]
>>> print(coord)
[[14, 14], [12, 11], [7, 8], [7, 11], [9, 16], [12, 14], [14, 3], [6, 7], [14, 14], [9, 3]]